package at.hakimst.daojunit.dataaccess;

import java.util.List;
import java.util.Optional;

public interface BaseRepository<T, I> {

    /**
     * insert a new entity, autogenerate ID
     * @param entity
     * @return new entity with ID
     */
    Optional<T> insert(T entity);

    /**
     * load a single entity by ID
     * @param id
     * @return
     */
    Optional<T> getById(I id);

    /**
     * get a list of all entities
     * @return
     */
    List<T> getAll();

    /**
     * update single entity
     * @param entity
     */
    void update(T entity);

    /**
     * delete single entity by ID
     * @param id
     * @return true if successful
     */
    boolean deleteById(I id);
}
