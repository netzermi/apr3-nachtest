package at.hakimst.daojunit.dataaccess;

import at.hakimst.daojunit.domain.Client;
import at.hakimst.daojunit.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.sql.*;

public class ClientRepository implements BaseRepository<Client, Long>{

    private Connection con;

    public ClientRepository() throws SQLException, ClassNotFoundException {
        this.con = H2DatabaseConnection.getConnection();
    }

    @Override
    public Optional<Client> insert(Client client) {
        //TODO

        return null;

    }

    @Override
    public Optional<Client> getById(Long id) {

        String sql = "SELECT * FROM `clients` WHERE id = ?";

        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return Optional.of(new Client(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getString("email")));
            }

            return Optional.empty();
        } catch (SQLException e) {
            throw new DatabaseException("Database error occured: " + e.getMessage());
        }
    }

    @Override
    public List<Client> getAll() {
     //TODO

        return null;
    }

    @Override
    public boolean deleteById(Long id) {
        Assert.notNull(id);
        String sql = "DELETE FROM clients WHERE id = ?";
        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setLong(1, id);
            preparedStatement.executeQuery();
            return true;
        }
        catch (SQLException e){
            throw new DatabaseException("Database error occured: " + e.getMessage());

        }


    }

    @Override
    public void update(Client entity) {
        Assert.notNull(entity);
        String sql = "UPDATE clients SET firstname = ?, lastname = ?, email = ? WHERE id = ?";

        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, entity.getFirstname());
            preparedStatement.setString(2, entity.getLastname());
            preparedStatement.setString(3, entity.getEmail());
            preparedStatement.setLong(4, entity.getId());

            preparedStatement.executeQuery();
        }
        catch (SQLException e){
            throw new DatabaseException("Database error occured: " + e.getMessage());
        }
    }

}
