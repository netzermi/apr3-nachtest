package at.hakimst.daojunit.dataaccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class H2DatabaseConnection {

    private static Connection con = null;

    private H2DatabaseConnection() {
    }

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        if (con != null) {
            return con;
        } else {
            Class.forName ("org.h2.Driver");
            con = DriverManager.getConnection("jdbc:h2:mem:clientdb");
            //con = DriverManager.getConnection("jdbc:h2:file:~/test");

            // create table at startup
            String sql = "CREATE TABLE clients (" +
                    "  id int NOT NULL PRIMARY KEY AUTO_INCREMENT ," +
                    "  firstname varchar(64) NOT NULL," +
                    "  lastname varchar(64) NOT NULL," +
                    "  email varchar(64) NOT NULL"  +
                    ");";

            Statement statement = con.createStatement();
            statement.execute(sql);

            // insert demo data
            statement.execute("INSERT INTO clients (firstname, lastname, email) VALUES ('Max', 'Mustermann', 'max@muster.de')");
            statement.execute("INSERT INTO clients (firstname, lastname, email) VALUES ('Susi', 'Sorglos', 'susi@sorglos.de')");
            return con;
        }
    }
}
