package at.hakimst.daojunit.dataaccess;

import at.hakimst.daojunit.domain.Client;
import org.junit.jupiter.api.*;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ClientRepositoryTest {

    private ClientRepository repository;

    @BeforeEach
    void setUp() {
        try {
            this.repository = new ClientRepository();
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @Order(1)
    void testGetAll() {
        //TODO Test if not null
    }

    @Test
    @Order(2)
    void getByID() {
        Client expected = new Client(1L,"Max","Mustermann","max@muster.de");
        Client actual = repository.getById(1L).get();
        assertEquals(expected,actual);
    }

    @Test
    @Order(3)
    void update() {
        Client client = repository.getById(1L).get();
        Client clientUpdate = new Client(1L, "Hans","Peter","hans@muster.de");
        repository.update(clientUpdate);
       assertNotEquals(clientUpdate, repository.getById(1L).get());
    }

    @Test
    @Order(4)
    void deleteByID() {
        //Test if size decreased
    }






}
